declare module "@salesforce/label/c.Acceptance_Label" {
    var Acceptance_Label: string;
    export default Acceptance_Label;
}
declare module "@salesforce/label/c.Closed_Funded_Label" {
    var Closed_Funded_Label: string;
    export default Closed_Funded_Label;
}
declare module "@salesforce/label/c.Closing_Label" {
    var Closing_Label: string;
    export default Closing_Label;
}
declare module "@salesforce/label/c.Compliance_Label" {
    var Compliance_Label: string;
    export default Compliance_Label;
}
declare module "@salesforce/label/c.Creating_Proposed_Debts" {
    var Creating_Proposed_Debts: string;
    export default Creating_Proposed_Debts;
}
declare module "@salesforce/label/c.Credit_Review_Label" {
    var Credit_Review_Label: string;
    export default Credit_Review_Label;
}
declare module "@salesforce/label/c.Default_App_Field" {
    var Default_App_Field: string;
    export default Default_App_Field;
}
declare module "@salesforce/label/c.Default_App_Field_Already_Exists" {
    var Default_App_Field_Already_Exists: string;
    export default Default_App_Field_Already_Exists;
}
declare module "@salesforce/label/c.Generate" {
    var Generate: string;
    export default Generate;
}
declare module "@salesforce/label/c.Generating_Proposed_Pricing_Options" {
    var Generating_Proposed_Pricing_Options: string;
    export default Generating_Proposed_Pricing_Options;
}
declare module "@salesforce/label/c.Group_With_Name" {
    var Group_With_Name: string;
    export default Group_With_Name;
}
declare module "@salesforce/label/c.Hold_Label" {
    var Hold_Label: string;
    export default Hold_Label;
}
declare module "@salesforce/label/c.Include_Secondary_Routes" {
    var Include_Secondary_Routes: string;
    export default Include_Secondary_Routes;
}
declare module "@salesforce/label/c.Object" {
    var Object: string;
    export default Object;
}
declare module "@salesforce/label/c.Picklist_Header_Namespace" {
    var Picklist_Header_Namespace: string;
    export default Picklist_Header_Namespace;
}
declare module "@salesforce/label/c.Proposal_Label" {
    var Proposal_Label: string;
    export default Proposal_Label;
}
declare module "@salesforce/label/c.Qualification_Label" {
    var Qualification_Label: string;
    export default Qualification_Label;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Account_Error_Msg" {
    var Risk_Rating_Selector_Account_Error_Msg: string;
    export default Risk_Rating_Selector_Account_Error_Msg;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Account_Title" {
    var Risk_Rating_Selector_Account_Title: string;
    export default Risk_Rating_Selector_Account_Title;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Back_To_Button" {
    var Risk_Rating_Selector_Back_To_Button: string;
    export default Risk_Rating_Selector_Back_To_Button;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Config_Instructions" {
    var Risk_Rating_Selector_Config_Instructions: string;
    export default Risk_Rating_Selector_Config_Instructions;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Configuration" {
    var Risk_Rating_Selector_Configuration: string;
    export default Risk_Rating_Selector_Configuration;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Finish" {
    var Risk_Rating_Selector_Finish: string;
    export default Risk_Rating_Selector_Finish;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Install" {
    var Risk_Rating_Selector_Install: string;
    export default Risk_Rating_Selector_Install;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Loan_Title" {
    var Risk_Rating_Selector_Loan_Title: string;
    export default Risk_Rating_Selector_Loan_Title;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Save_Button" {
    var Risk_Rating_Selector_Save_Button: string;
    export default Risk_Rating_Selector_Save_Button;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Save_URL_UI" {
    var Risk_Rating_Selector_Save_URL_UI: string;
    export default Risk_Rating_Selector_Save_URL_UI;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Success_Install" {
    var Risk_Rating_Selector_Success_Install: string;
    export default Risk_Rating_Selector_Success_Install;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Success_Uninstall" {
    var Risk_Rating_Selector_Success_Uninstall: string;
    export default Risk_Rating_Selector_Success_Uninstall;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Template_Error_Msg" {
    var Risk_Rating_Selector_Template_Error_Msg: string;
    export default Risk_Rating_Selector_Template_Error_Msg;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Title" {
    var Risk_Rating_Selector_Title: string;
    export default Risk_Rating_Selector_Title;
}
declare module "@salesforce/label/c.Risk_Rating_Selector_Uninstall" {
    var Risk_Rating_Selector_Uninstall: string;
    export default Risk_Rating_Selector_Uninstall;
}
declare module "@salesforce/label/c.Route_Configuration" {
    var Route_Configuration: string;
    export default Route_Configuration;
}
declare module "@salesforce/label/c.Route_With_Name" {
    var Route_With_Name: string;
    export default Route_With_Name;
}
declare module "@salesforce/label/c.Standard_Ncino_UI_Template_Example" {
    var Standard_Ncino_UI_Template_Example: string;
    export default Standard_Ncino_UI_Template_Example;
}
declare module "@salesforce/label/c.Tertiary_Only_Page_Template_Example" {
    var Tertiary_Only_Page_Template_Example: string;
    export default Tertiary_Only_Page_Template_Example;
}
declare module "@salesforce/label/c.UI_Template_Generator_Error_Message" {
    var UI_Template_Generator_Error_Message: string;
    export default UI_Template_Generator_Error_Message;
}
declare module "@salesforce/label/c.UI_Template_Generator_Error_Reason" {
    var UI_Template_Generator_Error_Reason: string;
    export default UI_Template_Generator_Error_Reason;
}
declare module "@salesforce/label/c.UI_Template_Generator_Header" {
    var UI_Template_Generator_Header: string;
    export default UI_Template_Generator_Header;
}
declare module "@salesforce/label/c.UI_Template_Generator_Help_Text" {
    var UI_Template_Generator_Help_Text: string;
    export default UI_Template_Generator_Help_Text;
}
declare module "@salesforce/label/c.UI_Template_Generator_Success_Message" {
    var UI_Template_Generator_Success_Message: string;
    export default UI_Template_Generator_Success_Message;
}
declare module "@salesforce/label/c.Underwriting_Label" {
    var Underwriting_Label: string;
    export default Underwriting_Label;
}
declare module "@salesforce/label/c.Visualforce_Page" {
    var Visualforce_Page: string;
    export default Visualforce_Page;
}
declare module "@salesforce/label/c.Visualforce_Page_Generator" {
    var Visualforce_Page_Generator: string;
    export default Visualforce_Page_Generator;
}
declare module "@salesforce/label/c.Withdrawn_Label" {
    var Withdrawn_Label: string;
    export default Withdrawn_Label;
}